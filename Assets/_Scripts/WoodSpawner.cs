﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PolySculptor
{
    public class WoodSpawner : MonoBehaviour
    {
        public Transform SpawnPoint;
        public Lever SpawnLever;
        public GameObject SpawnPrefab;

        private void Start()
        {
            SpawnLever.PulledDown += Spawn;
        }

        public void Spawn(Lever _ = null)
        {
            Instantiate(SpawnPrefab, SpawnPoint.position, SpawnPoint.rotation);
            SpawnLever.Break();
        }
    }
}