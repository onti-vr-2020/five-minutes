﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PolySculptor
{
    public class StoneSpawner : MonoBehaviour
    {
        public GameObject SpawnPrefab;
        public Transform SpawnPoint;

        public Collider ForceAffector;
        public Collider TargetVolume;

        private GameObject _spawned;

        private void Start() 
        {
            Spawn();
        }

        private void Update() {
            if(_spawned == null || !TargetVolume.bounds.Intersects(_spawned.GetComponent<Collider>().bounds)) {
                Spawn();
            }

            if (ForceAffector.bounds.Intersects(_spawned.GetComponent<Collider>().bounds)) {
                _spawned.GetComponent<Rigidbody>().isKinematic = true;
                _spawned.transform.position += SpawnPoint.forward * Time.deltaTime;
            } else {
                _spawned.GetComponent<Rigidbody>().isKinematic = false;
            }
        }

        private void Spawn()
        {
            _spawned = Instantiate(SpawnPrefab, SpawnPoint.position, SpawnPoint.rotation);
            _spawned.transform.parent = null;
        }
    }
}