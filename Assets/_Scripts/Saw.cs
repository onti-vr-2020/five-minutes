﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PolySculptor
{
    /// <summary>
    /// Saw tool. Can slice only wood parts.
    /// </summary>
    public class Saw : MonoBehaviour
    {
        public GameObject Handle;
        public Collider SawCollider;
        public Collider BladeCollider;
        public PolyCross PolyCross;

        private bool _equiped = false;
        private bool _locked = false;
        private GameObject _hovered;
        private GameObject _connected;
        private ConfigurableJoint _joint;
        private Interactable _interactable;

        private void Start()
        {
            _interactable = GetComponent<Interactable>();
            _interactable.Released += Released;
            _interactable.Connected += Connected;
            _interactable.PrimaryDown += PrimaryDown;

            _joint = GetComponent<ConfigurableJoint>();
            FreeJoint();
        }

        private void Update()
        {
            if (_locked && _equiped)
            {
                _joint.connectedAnchor -= this.transform.up * this.GetComponent<Rigidbody>().velocity.magnitude * 3 * Time.deltaTime;

                if (!BladeCollider.bounds.Intersects(_connected.GetComponent<Collider>().bounds))
                {
                    Finish();
                }
            }
        }

        private void Released()
        {
            _equiped = false;
        }

        private void Connected()
        {
            _equiped = true;
        }

        private void Finish()
        {
            _connected.GetComponent<WoodSliceable>().Slice(this.transform.position, this.transform.forward);

            SawCollider.isTrigger = false;
            _connected = null;
            _locked = false;
            FreeJoint();
        }

        private void FreeJoint()
        {
            _joint.xMotion = ConfigurableJointMotion.Free;
            _joint.yMotion = ConfigurableJointMotion.Free;
            _joint.zMotion = ConfigurableJointMotion.Free;
            _joint.angularXMotion = ConfigurableJointMotion.Free;
            _joint.angularYMotion = ConfigurableJointMotion.Free;
            _joint.angularZMotion = ConfigurableJointMotion.Free;

            _interactable.SnapTo = Handle;
        }

        private void PrepareJoint()
        {
            _joint.xMotion = ConfigurableJointMotion.Limited;
            _joint.yMotion = ConfigurableJointMotion.Locked;
            _joint.zMotion = ConfigurableJointMotion.Locked;
            _joint.angularXMotion = ConfigurableJointMotion.Locked;
            _joint.angularYMotion = ConfigurableJointMotion.Locked;
            _joint.angularZMotion = ConfigurableJointMotion.Locked;

            _interactable.SnapTo = null;
        }

        private void PrimaryDown()
        {
            if (!_locked && _hovered != null)
            {
                _locked = true;
                _connected = _hovered;
                _hovered = null;
                PrepareJoint();
                _joint.connectedBody = null;
                _joint.connectedAnchor = this.transform.position;
                _joint.secondaryAxis = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
                SawCollider.isTrigger = true;
                return;
            }

            if (_locked && _connected != null)
            {
                SawCollider.isTrigger = false;
                _connected = null;
                _locked = false;
                FreeJoint();
                return;
            }
        }

        void OnTriggerEnter(Collider other)
        {
            if (other.GetComponent<WoodSliceable>() != null)
            {
                _hovered = other.gameObject;
                var material = _hovered.GetComponent<MeshRenderer>().material;
                material.SetFloat("_CrossThickness", 1);
                PolyCross.PolyMaterial = material;

                if (_hovered.GetComponent<MeshRenderer>().materials.Length > 1)
                {
                    var additionalMaterial = _hovered.GetComponent<MeshRenderer>().materials[1];
                    PolyCross.AdditionalPolyMaterial = additionalMaterial;
                    additionalMaterial.SetFloat("_CrossThickness", 1);
                }
            }
        }
        void OnTriggerExit(Collider other)
        {
            if (other.gameObject == _hovered)
            {
                var material = _hovered.GetComponent<MeshRenderer>().material;
                material.SetFloat("_CrossThickness", 0);

                if (_hovered.GetComponent<MeshRenderer>().materials.Length > 1)
                {
                    var additionalMaterial = _hovered.GetComponent<MeshRenderer>().materials[1];
                    additionalMaterial.SetFloat("_CrossThickness", 0);
                }

                PolyCross.PolyMaterial = null;
                PolyCross.AdditionalPolyMaterial = null;
                _hovered = null;
            }
        }
    }
}