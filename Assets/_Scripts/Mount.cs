﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PolySculptor
{
    public class Mount : MonoBehaviour
    {
        public bool Mounted = false;
        public void TryHold()
        {
            var bounds = GetComponent<Collider>().bounds;
            var colliders = Physics.OverlapSphere(this.transform.position, 1, 1 << 11);

            bool collides = false;

            foreach (var collider in colliders)
            {
                if (collider.bounds.Intersects(bounds))
                {
                    collides = true;
                    break;
                }
            }

            if (!collides)
            {
                return;
            }

            this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
            this.gameObject.layer = 11;
            this.gameObject.tag = "Mounted";
            Mounted = true;
        }

        public void Unhold()
        {
            this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
            this.gameObject.layer = 0;
            this.gameObject.tag = "A";
            Mounted = false;
        }
    }

}