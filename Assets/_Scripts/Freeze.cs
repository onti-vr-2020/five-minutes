﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PolySculptor
{
    [RequireComponent(typeof(Rigidbody))]
    public class Freeze : MonoBehaviour
    {
        public SliceableType SliceableType;

        public Material SliceMaterial;
        void Start()
        {
            if (GetComponent<Sliceable>() != null)
            {
                SliceMaterial = new Material(GetComponent<Sliceable>().SliceMaterial);
                Destroy(GetComponent<Sliceable>());
            }
        }

        public void Hold()
        {
            this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
            Destroy(this.GetComponent<Interactable>());
            Sliceable sliceable;

            switch (SliceableType) {
                case SliceableType.Wood:
                    sliceable = this.gameObject.AddComponent<WoodSliceable>();
                    sliceable.SliceMaterial = SliceMaterial;
                    break;
                case SliceableType.Stone:
                    sliceable = this.gameObject.AddComponent<StoneSliceable>();
                    sliceable.SliceMaterial = SliceMaterial;
                    break;
            }
        }

        public void Unhold()
        {
            this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
            this.gameObject.AddComponent<Interactable>();
            Destroy(GetComponent<Sliceable>());
        }
    }
}