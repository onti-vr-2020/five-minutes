﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;

namespace PolySculptor
{
    public class ZeroGravity : MonoBehaviour
    {
        public Transform Point0;
        public Transform Point1;
        public Text ButtonText;

        private bool _active = false;
        private List<Freeze> freezed = new List<Freeze>();

        private void Start()
        {
            ButtonText.text = "FREEZE";
        }

        public void Toggle()
        {
            if (_active)
            {
                foreach (var freeze in freezed)
                {
                    freeze.Unhold();
                }

                freezed.Clear();
                ButtonText.text = "FREEZE";
            }
            else
            {
                freezed =
                    (from collider in Physics.OverlapCapsule(Point0.position, Point1.position, 1F)
                     where (collider.gameObject.GetComponent<Freeze>() != null)
                     select collider.gameObject.GetComponent<Freeze>()).ToList();

                foreach (var freeze in freezed)
                {
                    freeze.Hold();
                }

                ButtonText.text = "UNFREEZE";
            }

            _active = !_active;
        }
    }
}