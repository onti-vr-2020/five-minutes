﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PolySculptor
{
    [System.Serializable]
    public struct GameLevel
    {
        public string Name;
        public Vector3 WireframeOffset;
        public GameObject Wireframe;
        public float Accuracy;
        public Sprite Preview;
    }

    public class PolyWireframe : MonoBehaviour
    {
        public Lever EvaluateLever;
        public Lever ExitLever;
        public GameLevel[] Levels;

        public string DetailsTag = "Mounted";
        public GameObject TargetWireframe;

        public GameObject UI_Selector;
        public GameObject UI_Mark;
        public Text UI_MarkText;
        public Text UI_SelectorMarkText;
        public Text UI_SelectorLevelName;
        public Image UI_SelectorPreview;

        private int chosenLevel = 0;
        private int currentLevel = 0;
        
        public void NextLevel()
        {
            if (chosenLevel < Levels.Length - 1)
                chosenLevel++;
            UpdateUI();
        }

        public void PrevLevel()
        {
            if (chosenLevel > 0)
                chosenLevel--;
            UpdateUI();
        }

        public void Evaluate(Lever lever)
        {
            StartCoroutine(EvaluateFinal());
            EvaluateLever.Break();
        }

        public IEnumerator EvaluateFinal()
        {
            if (currentLevel == -1)
                yield break;
            var details = GameObject.FindGameObjectsWithTag(DetailsTag);
            if (details.Length == 0)
                yield break;
            var mc_details = new List<Collider>();
            var mc_wireframe = TargetWireframe.GetComponent<MeshCollider>();
            mc_wireframe.isTrigger = false;
            mc_wireframe.convex = false;
            var result = new PPCalc<float>();
            StartCoroutine(PolyPhysics.GetJaccardCoefficientAsync(mc_wireframe, mc_details, result));
            UI_Selector.SetActive(false);
            UI_Mark.SetActive(true);
            UI_MarkText.text = "Evaluating...\nPlease wait";
            while (!result.Finished)
            {
                Debug.Log("FDF");
                yield return new WaitForEndOfFrame();
            }
            var acc = result.GetResult();
            var grade = GetGrade(acc);
            UI_MarkText.text = $"<size=200>{grade}</size>\n{(int)(acc * 100)}%";
            Levels[currentLevel].Accuracy = acc;
            mc_wireframe.convex = true;
            mc_wireframe.isTrigger = true;
            
        }

        public void UpdateUI()
        {
            float acc = Levels[currentLevel].Accuracy;
            UI_Selector.SetActive(true);
            UI_Mark.SetActive(false);
            UI_SelectorLevelName.text = Levels[currentLevel].Name;
            UI_SelectorMarkText.text = $"{GetGrade(acc)}\n<size=70>{(int)(acc * 100)}%</size>";
        }

        public void LoadLevel()
        {
            var lvl = Levels[chosenLevel];
            if (transform.childCount > 0)
                Destroy(transform.GetChild(0).gameObject);
            var wireframe = Instantiate(lvl.Wireframe, transform);
            wireframe.transform.localPosition = lvl.WireframeOffset;
            TargetWireframe = wireframe;
            currentLevel = chosenLevel;
        }

        public void Exit(Lever foo)
        {
            Application.Quit();
            ExitLever.Break();
        }

        public void Update()
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
                LoadLevel();
            if (Input.GetKey(KeyCode.Alpha2))
                StartCoroutine(EvaluateFinal());
        }

        public void Start()
        {
            EvaluateLever.PulledDown += Evaluate;
            ExitLever.PulledDown += Exit;

            UpdateUI();
            LoadLevel();
        }

        public char GetGrade(float acc)
        {
            if (acc * 100 > 90)
                return 'S';
            if (acc * 100 > 80)
                return 'A';
            if (acc * 100 > 70)
                return 'B';
            if (acc * 100 > 50)
                return 'C';
            return 'D';
        }
    }
}