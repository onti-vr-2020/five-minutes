﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PolySculptor
{
    public class Scissors : MonoBehaviour
    {
        public GameObject MovingPart;

        private Interactable _interactable;

        private void Start()
        {
            _interactable = GetComponent<Interactable>();
        }

        private void Update()
        {
            if (_interactable.Value == null)
                return;

            var localRot = MovingPart.transform.localEulerAngles;
            var target = Mathf.Lerp(45F, 0F, _interactable.Value.GetAxis(_interactable.Hand));
            
            localRot.y = Mathf.Lerp(localRot.y, target, Time.deltaTime * 10);
            MovingPart.transform.localEulerAngles = localRot;
        }
    }
}