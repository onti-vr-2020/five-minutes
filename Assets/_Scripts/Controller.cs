﻿using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

namespace PolySculptor
{
    /// <summary>
    /// Behaviour <c>Controller</c> replaces a default SteamVR Interaction System.
    /// All interactions are physics-driven.
    /// </summary>
    [RequireComponent(typeof(Collider))]
    public class Controller : MonoBehaviour
    {
        /// <value>
        /// Property <c>Hand</c> represents a SteamVR Input Source of a hand to which this controller added.
        /// </value>
        public SteamVR_Input_Sources Hand;

        /// <value>
        /// Property <c>ControllerPose</c> represents a SteamVR Behaviour Pose of a hand to which this controller added.
        /// </value>
        public SteamVR_Behaviour_Pose ControllerPose;

        /// <value>
        /// Property <c>ControllerPose</c> represents a SteamVR Action that controls whether Controller will keep an object grabbed.
        /// </value>
        public SteamVR_Action_Boolean GrabAction;
        /// <value>
        /// Property <c>ControllerPose</c> represents a SteamVR Action that trigger an Interactive object primary action.
        /// </value>
        public SteamVR_Action_Boolean ObjectAction;
        /// <value>
        /// Property <c>ControllerPose</c> represents a SteamVR Single that controlls an Interactive object primary value.
        /// </value>
        public SteamVR_Action_Single ObjectValue;

        private GameObject _connectedObject;

        private GameObject _hoveredObject;

        private void Update()
        {
            if (GrabAction.GetLastStateDown(Hand))
            {
                if (_hoveredObject)
                {
                    GrabObject();
                }
            }

            if (GrabAction.GetLastStateUp(Hand))
            {
                if (_connectedObject)
                {
                    ReleaseObject();
                }
            }

            if (_connectedObject)
            {
                if (ObjectAction.GetLastStateDown(Hand))
                {
                    if (_connectedObject.GetComponent<Interactable>().PrimaryDown != null)
                        _connectedObject.GetComponent<Interactable>().PrimaryDown();
                }

                if (ObjectAction.GetLastStateUp(Hand))
                {
                    if (_connectedObject.GetComponent<Interactable>().PrimaryUp != null)
                        _connectedObject.GetComponent<Interactable>().PrimaryUp();
                }
            }
        }

        private void GrabObject()
        {
            _connectedObject = _hoveredObject;
            _hoveredObject = null;

            if (_connectedObject.GetComponent<Interactable>().SnapTo != null)
            {
                _connectedObject.transform.rotation = this.transform.rotation;
                _connectedObject.transform.rotation *= Quaternion.Inverse(_connectedObject.GetComponent<Interactable>().SnapTo.transform.localRotation);
                _connectedObject.transform.position = this.transform.position;
                _connectedObject.transform.position += _connectedObject.transform.position - _connectedObject.GetComponent<Interactable>().SnapTo.transform.position;
            }

            var joint = AddFixedJoint();
            joint.connectedBody = this.GetComponent<Rigidbody>();

            if (_connectedObject.GetComponent<Interactable>().Connected != null)
                _connectedObject.GetComponent<Interactable>().Connected();

            _connectedObject.GetComponent<Interactable>().Value = ObjectValue;
            _connectedObject.GetComponent<Interactable>().Hand = Hand;
        }

        private FixedJoint AddFixedJoint()
        {
            FixedJoint fx = _connectedObject.AddComponent<FixedJoint>();
            fx.breakForce = 150000;
            fx.breakTorque = 150000;
            return fx;
        }

        private void ReleaseObject()
        {
            if (_connectedObject.GetComponent<FixedJoint>())
            {
                _connectedObject.GetComponent<FixedJoint>().connectedBody = null;
                Destroy(_connectedObject.GetComponent<FixedJoint>());

                _connectedObject.GetComponent<Rigidbody>().velocity =
                    ControllerPose.GetVelocity();
                _connectedObject.GetComponent<Rigidbody>().angularVelocity =
                    ControllerPose.GetAngularVelocity();

                if (_connectedObject.GetComponent<Interactable>() != null)
                {
                    if (_connectedObject.GetComponent<Interactable>().Released != null)
                        _connectedObject.GetComponent<Interactable>().Released();

                    _connectedObject.GetComponent<Interactable>().Value = null;
                    _connectedObject.GetComponent<Interactable>().Hand = SteamVR_Input_Sources.Any;
                }
            }
            _connectedObject = null;
        }

        void OnTriggerEnter(Collider other)
        {
            if (other.GetComponent<Interactable>() != null)
            {
                _hoveredObject = other.gameObject;
            }
        }

        void OnTriggerExit(Collider other)
        {
            if (other.gameObject == _hoveredObject)
            {
                _hoveredObject = null;
            }
        }
    }
}
