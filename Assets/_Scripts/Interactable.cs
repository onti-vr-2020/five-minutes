using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

namespace PolySculptor
{
    /// <summary>
    /// Behaviour <c>Interactable</c> makes the object grabbable.
    /// </summary>
    [RequireComponent(typeof(Rigidbody))]
    public class Interactable : MonoBehaviour
    {
        public GameObject SnapTo;
        [HideInInspector]
        public SteamVR_Action_Single Value;
        [HideInInspector]
        public SteamVR_Input_Sources Hand;

        public delegate void ReleasedHandler();
        public delegate void ConnectedHandler();
        public delegate void PrimaryDownHandler();
        public delegate void PrimaryUpHandler();

        public ReleasedHandler Released;
        public ConnectedHandler Connected;
        public PrimaryDownHandler PrimaryDown;
        public PrimaryUpHandler PrimaryUp;
    }
}
