﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PolySculptor
{
    public class MeasuringTape : MonoBehaviour
    {
        private enum State
        {
            None,
            Measuring,
            Measured,
        }

        public Transform TapeSource;
        public Text Display;
        public Material TapeMaterial;

        private Interactable _interactable;
        private State _state = State.None;
        private GameObject _start;
        private GameObject _end;

        private void Start()
        {
            _interactable = GetComponent<Interactable>();

            _interactable.Released += Released;
            _interactable.PrimaryDown += StartMeasure;
            _interactable.PrimaryUp += StopMeasure;

            Display.text = "0.0";
        }

        private void StartMeasure()
        {
            if (_state == State.Measured)
            {
                Destroy(_end);
                Destroy(_start);
                Display.text = "0.0";
            }

            _state = State.Measuring;

            _start = new GameObject("Measure Start");
            _start.transform.position = TapeSource.position;
            _start.transform.rotation = TapeSource.rotation;
            _start.transform.parent = null;

            var lineRenderer = _start.AddComponent<LineRenderer>();
            lineRenderer.positionCount = 2;
            lineRenderer.SetPosition(0, _start.transform.position);
            lineRenderer.SetPosition(1, TapeSource.position);
            lineRenderer.startWidth = 0.03F;
            lineRenderer.endWidth = 0.03F;
            lineRenderer.material = TapeMaterial;
        }

        private void StopMeasure()
        {
            if (_state != State.Measuring)
                return;

            _state = State.Measured;

            _end = new GameObject("Measure End");
            _end.transform.position = TapeSource.position;
            _end.transform.rotation = TapeSource.rotation;
            _end.transform.parent = null;
            _start.GetComponent<LineRenderer>().SetPosition(1, _end.transform.position);

            Measure(_end.transform);
        }

        private void Released()
        {
            Destroy(_start);
            Destroy(_end);
            Display.text = "0.0";
        }

        private void Measure(Transform target)
        {
            float dist = Vector3.Distance(_start.transform.position, target.position);
            Display.text = String.Format("{0:0.0}", dist);
        }

        private void Update()
        {
            if (_state == State.Measuring)
            {
                _start.GetComponent<LineRenderer>().SetPosition(1, TapeSource.position);
                Measure(TapeSource);
            }
        }
    }
}