﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PolySculptor
{
    public class PPCalc<T> where T : struct
    {
        public bool Finished = false;
        private T result;

        public void UploadResult(T val)
        {
            this.result = val;
            this.Finished = true;
        }
        public T GetResult() => this.result;
    }

    public static class PolyPhysics
    {

        public const int JACCARD_RESOLUTION = 100;
        public const float JACCARD_RAY_DISTANCE = 100f;
        public const int JACCARD_DELAYS_MOD = 10000;
        public const int CUSTOM_LAYER = 9;

        public const float CUT_KINEMATIC_DELAY = 0.5f;

        public static IEnumerator CalculateCutPhysicsAsync(GameObject First, GameObject Second, float initialMass)
        {
            First.GetComponent<Rigidbody>().isKinematic = true;
            Second.GetComponent<Rigidbody>().isKinematic = true;

            yield return new WaitForSeconds(CUT_KINEMATIC_DELAY);

            var mc_first = First.GetComponent<MeshCollider>();
            var mc_second = Second.GetComponent<MeshCollider>();
            var rb_first = First.GetComponent<Rigidbody>();
            var rb_second = Second.GetComponent<Rigidbody>();

            rb_first.isKinematic = true;
            rb_second.isKinematic = true;

            Vector3 center_first = Vector3.zero;
            Vector3 center_second = Vector3.zero;
            foreach (var v in mc_first.sharedMesh.vertices)
                center_first += v;
            foreach (var v in mc_second.sharedMesh.vertices)
                center_second += v;
            center_first /= mc_first.sharedMesh.vertices.Length;
            center_second /= mc_second.sharedMesh.vertices.Length;
            rb_first.centerOfMass = center_first;
            rb_second.centerOfMass = center_second;

            float dcoef_first = 1;
            float dcoef_second = 1;

            foreach (var v in mc_first.sharedMesh.vertices)
                dcoef_first *= Vector3.Distance(v, center_first);
            foreach (var v in mc_second.sharedMesh.vertices)
                dcoef_second *= Vector3.Distance(v, center_second);

            float masscoef_first = dcoef_first / (dcoef_first + dcoef_second);
            float masscoef_second = 1.0f - masscoef_first;

            float mass_first = masscoef_first * initialMass;
            float mass_second = masscoef_second * initialMass;

            if (masscoef_first < 0.001f || masscoef_first > 0.999f)
            {
                masscoef_first = 0.5f;
                masscoef_second = 0.5f;
            }
            else
            {
                rb_first.mass = mass_first;
                rb_second.mass = mass_second;
            }

            rb_first.isKinematic = false;
            rb_second.isKinematic = false;
        }

        public static void CalculateCutPhysics(GameObject First, GameObject Second, float initialMass)
        {
            var mc_first = First.GetComponent<MeshCollider>();
            var mc_second = Second.GetComponent<MeshCollider>();
            var rb_first = First.GetComponent<Rigidbody>();
            var rb_second = Second.GetComponent<Rigidbody>();
            Vector3 center_first = Vector3.zero;
            Vector3 center_second = Vector3.zero;
            foreach (var v in mc_first.sharedMesh.vertices)
                center_first += v;
            foreach (var v in mc_second.sharedMesh.vertices)
                center_second += v;
            center_first /= mc_first.sharedMesh.vertices.Length;
            center_second /= mc_second.sharedMesh.vertices.Length;
            rb_first.centerOfMass = center_first;
            rb_second.centerOfMass = center_second;

            float dcoef_first = 1;
            float dcoef_second = 1;

            foreach (var v in mc_first.sharedMesh.vertices)
                dcoef_first *= Vector3.Distance(v, center_first);
            foreach (var v in mc_second.sharedMesh.vertices)
                dcoef_second *= Vector3.Distance(v, center_second);

            float masscoef_first = dcoef_first / (dcoef_first + dcoef_second);
            float masscoef_second = 1.0f - masscoef_first;

            float mass_first = masscoef_first * initialMass;
            float mass_second = masscoef_second * initialMass;

            Debug.Log(mass_first + " " + mass_second);

            rb_first.mass = mass_first;
            rb_second.mass = mass_second;
        }

        public static IEnumerator GetJaccardCoefficientAsync(MeshCollider wireframe, List<Collider> objects, PPCalc<float> result)
        {
            var lowerBound = Vector3.positiveInfinity;
            var upperBound = Vector3.negativeInfinity;

            foreach (var c in objects)
            {
                lowerBound = Vector3.Min(c.bounds.min, lowerBound);
                upperBound = Vector3.Max(c.bounds.max, upperBound);
                c.gameObject.layer = 0;
            }

            lowerBound = Vector3.Min(wireframe.bounds.min, lowerBound);
            upperBound = Vector3.Max(wireframe.bounds.max, upperBound);
            wireframe.gameObject.layer = 0;

            var nodeSize = (upperBound - lowerBound) / JACCARD_RESOLUTION;
            var nodeVolume = nodeSize.x * nodeSize.y * nodeSize.z;

            float volumeIntersection = 0;
            float volumeUnion = 0;

            var i = Vector3Int.zero;

            for (int ix = 0; ix < JACCARD_RESOLUTION; ix++)
            {
                for (int iy = 0; iy < JACCARD_RESOLUTION; iy++)
                {
                    for (int iz = 0; iz < JACCARD_RESOLUTION; iz++)
                    {
                        
                        i = new Vector3Int(ix, iy, iz);
                        int index = i.z + i.y * JACCARD_RESOLUTION + i.x * JACCARD_RESOLUTION * JACCARD_RESOLUTION;
                        var nodePos = Vector3.Scale(i, nodeSize) + nodeSize / 2.0f + lowerBound;
                        bool insideList = CheckVertexListInsideBox(objects, nodePos, nodeSize);
                        bool insideWireframe = CheckVertexInsideBox(wireframe, nodePos, nodeSize);
                        if (insideList && insideWireframe)
                        {
                            volumeIntersection += nodeVolume;
                            volumeUnion += nodeVolume;
                            //DrawBox(nodePos, nodeSize);
                        }
                        else if (insideList || insideWireframe)
                        {
                            volumeUnion += nodeVolume;
                            //DrawBox(nodePos, nodeSize);
                        }
                        if (index % JACCARD_DELAYS_MOD == 0)
                            yield return new WaitForEndOfFrame();
                    }
                }
            }

            result.UploadResult(volumeIntersection / volumeUnion);
        }

        private static void DrawBox(Vector3 pos, Vector3 size)
        {
            var box = GameObject.CreatePrimitive(PrimitiveType.Cube);
            box.transform.localScale = size;
            box.transform.position = pos;
        }

        public static float GetJaccardCoefficient(MeshCollider wireframe, List<Collider> objects)
        {
            var lowerBound = Vector3.positiveInfinity;
            var upperBound = Vector3.negativeInfinity;

            foreach (var c in objects)
            {
                lowerBound = Vector3.Min(c.bounds.min, lowerBound);
                upperBound = Vector3.Max(c.bounds.max, upperBound);
                c.gameObject.layer = 0;
            }

            lowerBound = Vector3.Min(wireframe.bounds.min, lowerBound);
            upperBound = Vector3.Max(wireframe.bounds.max, upperBound);
            wireframe.gameObject.layer = 0;

            var nodeSize = (upperBound - lowerBound) / JACCARD_RESOLUTION;
            var nodeVolume = nodeSize.x * nodeSize.y * nodeSize.z;

            float volumeIntersection = 0;
            float volumeUnion = 0;

            var i = Vector3Int.zero;

            for (int ix = 0; ix < JACCARD_RESOLUTION; ix++)
            {
                for (int iy = 0; iy < JACCARD_RESOLUTION; iy++)
                {
                    for (int iz = 0; iz < JACCARD_RESOLUTION; iz++)
                    {
                        i = new Vector3Int(ix, iy, iz);
                        var nodePos = Vector3.Scale(i, nodeSize) + nodeSize / 2.0f + lowerBound;
                        bool insideList = CheckVertexListInsideBox(objects, nodePos, nodeSize);
                        bool insideWireframe = CheckVertexInsideBox(wireframe, nodePos, nodeSize);
                        if (insideList && insideWireframe)
                        {
                            volumeIntersection += nodeVolume;
                            volumeUnion += nodeVolume;
                            //DrawBox(nodePos, nodeSize);
                        }
                        else if (insideList || insideWireframe)
                        {
                            volumeUnion += nodeVolume;
                            //DrawBox(nodePos, nodeSize);
                        }
                    }
                }
            }

            return volumeIntersection / volumeUnion;
        }

        private static bool CheckVertexListInsideBox(List<Collider> colliders, Vector3 v, Vector3 size)
        {
            foreach (var c in colliders)
            {
                c.gameObject.layer = 9;
                var result = Physics.CheckBox(v, size / 2.0f, Quaternion.identity, 1 << 9);
                c.gameObject.layer = 0;
                if (!result)
                    return false;
            }
            return true;
        }

        private static bool CheckVertexInsideBox(MeshCollider collider, Vector3 v, Vector3 size)
        {
            collider.gameObject.layer = 9;
            bool result = Physics.CheckBox(v, size / 2.0f, Quaternion.identity, 1 << 9);
            collider.gameObject.layer = 0;
            return result;
        }

    }
}