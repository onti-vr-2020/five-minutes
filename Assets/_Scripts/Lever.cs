﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PolySculptor
{
    public class Lever : MonoBehaviour
    {
        public delegate void Pulled(Lever lever);
        public Pulled PulledDown;

        public float Limit = 0.3F;
        private ConfigurableJoint joint;
        public GameObject Handle;
        public bool isPulled = false;

        void Start()
        {
            joint = GetComponentInChildren<ConfigurableJoint>();
            var softJointLimit = new SoftJointLimit();
            softJointLimit.limit = Limit;

            joint.linearLimit = softJointLimit;
            joint.targetPosition = new Vector3(Limit, 0, 0);
        }

        void Update()
        {
            if (Handle.transform.localPosition.x < -(Limit-0.06) && !isPulled)
            {
                joint.targetPosition = new Vector3(-Limit, 0, 0);
                Destroy(Handle.GetComponent<Interactable>());
                isPulled = true;

                if (PulledDown != null)
                    PulledDown(this);
            }

            if (Handle.transform.localPosition.x > 0)
            {
                isPulled = false;
            }
        }

        public void Break()
        {
            joint.targetPosition = new Vector3(Limit, 0, 0);
            Handle.AddComponent(typeof(Interactable));
        }
    }
}