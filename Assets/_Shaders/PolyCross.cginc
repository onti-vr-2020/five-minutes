﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

#ifndef POLYCROSS_CG_INCLUDED
#define POLYCROSS_CG_INCLUDED

float get_plane_distance(float4 plane, float3 vertex) {
    float d = abs((plane.x * vertex.x + plane.y * vertex.y + plane.z * vertex.z + plane.w));
    float e = sqrt(plane.x * plane.x + plane.y * plane.y + plane.z * plane.z);
    return (d / e);
}

float get_plane_distance_nonabs(float4 plane, float3 vertex) {
    float d = (plane.x * vertex.x + plane.y * vertex.y + plane.z * vertex.z + plane.w);
    float e = sqrt(plane.x * plane.x + plane.y * plane.y + plane.z * plane.z);
    return (d / e);
}

float3 project_point_onto_plane(float4 plane, float3 vertex) {
    float d = (plane.x * vertex.x + plane.y * vertex.y + plane.z * vertex.z + plane.w);
    float e = sqrt(plane.x * plane.x + plane.y * plane.y + plane.z * plane.z);
    return vertex + plane.xyz * (d / e);
}

float3 project_vec_onto_vec(float3 a, float3 b) {
    float len_b = dot(b, b);
    return dot(a, b) * b / len_b;
}

fixed get_cross_factor(float4 plane, float3 vertex, float3 normal) {
    float3 worldNormal = mul(unity_ObjectToWorld, float4(normal, 0.0)).xyz;
    float d = get_plane_distance(plane, vertex);

    //It's time for shader magic!
    float dt = abs(dot(plane.yxz, normal)); //normal
    dt *= dt * dt * dt * dt;
    dt = 1 - dt;
    //Just don't ask. Even don't try to.

    float m = 20;
    float3 coplanar = normalize(cross(worldNormal, plane));
    float3 projected = project_vec_onto_vec(coplanar, vertex);
    float dotline = length(projected);
    //return worldNormal.y;
    //dotline = coplanar.x;
    //dotline = dot(vertex, coplanar);
    dotline = dotline * m - round(dotline * m);
    //return dotline;
    return saturate(sign(1 - d * 100 / dt));
}

#endif